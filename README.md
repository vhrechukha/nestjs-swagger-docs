### nestjs-swagger-docs

Project with example of swagger auto-generation docs in NodeJs and deploy it to GitLab Pages. 

### Installation

```
npm install
npm start
```

### Running

Once the application is running you can visit [http://localhost:3000/api](http://localhost:3000/api) to see the Swagger interface.

Project example is cloned from [11-swagger sample](https://github.com/nestjs/nest/tree/master/sample/11-swagger) of [Nest](https://github.com/nestjs/nest) Framework.
