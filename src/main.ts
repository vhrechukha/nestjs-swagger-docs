import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as fs from 'fs';
import { AppModule } from './app.module';
import * as YAML from 'json-to-pretty-yaml';

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule);

    const options = new DocumentBuilder()
      .setTitle('Cats example')
      .setDescription('The cats API description')
      .setVersion('1.0')
      .addTag('cats')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api', app, document);

    const data = YAML.stringify(document);

    await fs.writeFileSync('swagger.yaml', data);
    new Logger().log('Swagger updated ✅\n');

    await app.listen(3000);
    new Logger().log(`Application is running on: ${await app.getUrl()}`);
  } catch(e) {
    new Logger().error(e);
  }
}
bootstrap();
